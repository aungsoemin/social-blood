class Township < ActiveRecord::Base
  validates_presence_of :name, :state_id

  has_many :users
  belongs_to :state_divisions

  reverse_geocoded_by :latitude, :longitude
end
