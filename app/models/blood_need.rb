class BloodNeed < ActiveRecord::Base

  belongs_to :blood_types
  belongs_to :blood_rh_types

  attr_accessor :distance, :city_name

  reverse_geocoded_by :latitude, :longitude

end
