class FacebookBot

  def self.handle_incoming_message(event, sender)
    begin
      message = event[:message][:text]

      if message.include?("/location")
        location = message.split("/location")[1].lstrip!
        self.handle_blood_bank_location(event, sender, location)
      elsif message.include?("/blood")
        blood_type = message.split("/blood")[1].lstrip!
        self.handle_blood_type(event, sender, blood_type)
      else
        FacebookBot.send_welcome_message(event, sender)
      end
    rescue
      self.reply_simple_message(event, sender, "We'll get back to you shortly.")
    end

  end

  def self.handle_payload(event, sender)
    payload = event[:postback][:payload]
    puts event
    puts event[:postback]
    if payload == "bloodbank"
      # Ask for user location
      self.ask_user_for_location(event, sender)
    elsif payload == "match"
      self.ask_user_for_bloodtype(event, sender)
    else
      self.ask_user_for_bloodtype(event, sender)
    end
  end

  def self.ask_user_for_location(event, sender)
    self.reply_simple_message(event, sender, "Where do you want to search for blood banks? (E.g. /location 1 Hacker Way)")
  end

  def self.handle_blood_bank_location(event, sender, location)
    begin
      # Search for blood bank
      bloodbanks = BloodBank.near(location).limit(8)

      if bloodbanks.count > 0
        message = "Here are some blood banks around #{location} \n"
        bloodbanks.each do |bloodbank|
          message = message + "#{bloodbank.name}\n#{bloodbank.address}\n#{bloodbank.phone_no}\n\n"
        end
        self.reply_simple_message(event, sender, message)
      else
        self.reply_simple_message(event, sender, "There are no blood banks available on our platform in that area.")
      end
    rescue
      self.reply_simple_message(event, sender, "There are no blood banks available on our platform in that area.")
    end
  end

  def self.ask_user_for_bloodtype(event, sender)
    self.reply_simple_message(event, sender, "What is the blood type that you are looking for? (Type /blood A+)")
  end

  def self.handle_blood_type(event, sender, blood_type_input)
    blood_type_input.upcase!
    begin
      blood_type_id = BloodType.where(name: blood_type_input[0]).first.id
      blood_rh_type_id = 3

      if blood_type_input[1] != nil
        sign = ""
        if blood_type_input[1] == "+"
          sign = "plus"
        else
          sign = "minus"
        end
        blood_rh_type_id = BloodRhType.where(name: sign).first.id
      end

      users = User.where(blood_type_id: blood_type_id, blood_rh_type_id: blood_rh_type_id).order("updated_at desc").limit(5)

      elements = []
      users.each do |user|
        temp = {
          title: user.address.present? ? "Near #{user.address}" : 'Unknown address',
          image_url: user.profile_photo.present? ? user.profile_photo.url : '',
          buttons: [
            {
              type: "web_url",
              title: "Call",
              url: "http://social-blood.herokuapp.com/call/#{user.phone_no}"
            },
            {
              type: "web_url",
              title: "Message",
              url: "http://social-blood.herokuapp.com/sms/#{user.phone_no}"
            }
          ]
        }
        elements << temp
      end

      data = {
        recipient: {
          id: event[:sender][:id]
        },
        message: {
          attachment: {
            type: "template",
            payload: {
              template_type: "generic",
              elements: elements
            }
          }
        }
      }

      self.post("https://graph.facebook.com/v2.6/me/messages?access_token=#{Messenger::Bot::Config.access_token}", data)
    rescue
      self.reply_simple_message(event, sender, "There are no donors with that blood type.")
    end
  end

  def self.reply_simple_message(event, sender, message)
    data = {
      recipient: {
        id: event[:sender][:id]
      },
      message: {
        text: message
      }
    }
    self.post("https://graph.facebook.com/v2.6/me/messages?access_token=#{Messenger::Bot::Config.access_token}", data)
  end

  def self.send_welcome_message(event, sender)
    data = {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [
            {
              title: "Welcome to Socialblood Bot!",
              image_url: "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p320x320/13043267_1006151539473136_1075215809877556234_n.png?oh=7667d1a8996e096ca0f74a812204e8ae&oe=57A6CD53&__gda__=1471425457_f06e0726620e510ff9460f543db88df8",
              subtitle: "Please select the following options to get started!",
              buttons: [
                {
                  type: "postback",
                  title: "Search blood bank",
                  payload: "bloodbank"
                },
                {
                  type: "postback",
                  title: "Find donor",
                  payload: "match"
                }
              ]
            }
          ]
        }
      }
    }
    sender.reply(data)
  end

  def self.send_one_time_welcome_message(event, sender)
    page_id = "117002108388088"
    data = {
      setting_type: "call_to_actions",
      thread_state: "new_thread",
      call_to_actions: [
        {
          message: {
            attachment: {
              type: "template",
              payload: {
                template_type: "generic",
                elements: [
                  {
                    title: "Welcome to Socialblood Bot!",
                    image_url: "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtp1/v/t1.0-1/p320x320/13043267_1006151539473136_1075215809877556234_n.png?oh=7667d1a8996e096ca0f74a812204e8ae&oe=57A6CD53&__gda__=1471425457_f06e0726620e510ff9460f543db88df8",
                    subtitle: "Please select the following options to get started!",
                    buttons: [
                      {
                        type: "postback",
                        title: "Search blood bank",
                        payload: "bloodbank"
                      },
                      {
                        type: "postback",
                        title: "Find donor",
                        payload: "match"
                      }
                    ]
                  }
                ]
              }
            }
          }
        }
      ]
    }
    self.post("https://graph.facebook.com/v2.6/#{page_id}/thread_settings?access_token=#{Messenger::Bot::Config.access_token}", data)
  end

  def self.post(url, data)
    url = URI.parse(url)
    http = Net::HTTP.new(url.host, 443)
    http.use_ssl = true
    begin
      request = Net::HTTP::Post.new(url.request_uri)
      request["Content-Type"] = "application/json"
      request.body = data.to_json
      response = http.request(request)
      body = JSON(response.body)
      return { ret: body["error"].nil?, body: body }
    rescue => e
      raise e
    end
  end

  def self.get(url, data = {})
    url = URI.parse(url)
    http = Net::HTTP.new(url.host, 443)
    http.use_ssl = true
    begin
      request = Net::HTTP::Get.new(url.request_uri)
      request["Content-Type"] = "application/json"
      response = http.request(request)
      body = JSON(response.body)
      return { ret: body["error"].nil?, body: body }
    rescue => e
      raise e
    end
  end

end
