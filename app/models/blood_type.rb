class BloodType < ActiveRecord::Base
  validates_presence_of :name

  has_many :users
  has_many :blood_needs
end
