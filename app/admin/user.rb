ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
permit_params :name, :email, :phone_no, :birth_date, :gender, :profile_photo, :blood_type_id, :blood_rh_type_id, :state_id, :township_id, :fb_id, :auth_token, :address, :latitude, :longitude, :has_pet, :pet

  index do
    selectable_column
    id_column
    column :id
    column :name
    column :email
    column :phone_no
    column :address
    column :latitude
    column :longitude
    column :birth_date
    column :gender
    column :profile_photo
    column :blood_type_id
    column :blood_rh_type_id
    actions
  end

  filter :name
  filter :email
  filter :phone_no
  filter :birth_date
  filter :gender
  filter :profile_photo
  filter :blood_type_id
  filter :blood_rh_type_id
  filter :address
  filter :latitude
  filter :longitude

  form do |f|
    f.inputs "New User" do
      f.input :name
      f.input :email
      f.input :phone_no
      f.input :birth_date
      f.input :gender
      f.input :address
      f.input :latitude
      f.input :longitude
      f.input :profile_photo, :as => :file
      f.input :blood_type_id, :as => :select, collection: BloodType.all.map {|b| [b.name, b.id]}
      f.input :blood_rh_type_id, :as => :select, collection: BloodRhType.all.map {|b| [b.name, b.id]}
    end
    f.actions
  end

  show do
      attributes_table do
        row :name
        row :email
        row :phone_no
        row :birth_date
        row :gender
        row :address
        row :latitude
        row :longitude
        row :profile_photo do
          image_tag (user.profile_photo.thumb)
        end
        row :about
        row :has_pet
        row :pet
        row :blood_type_id
        row :blood_rh_type_id
        row :fb_id
        row :auth_token

      end
      active_admin_comments
  end

end
