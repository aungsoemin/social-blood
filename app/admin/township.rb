ActiveAdmin.register Township do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
permit_params :name, :state_id, :latitude, :longitude

  index do
    selectable_column
    id_column
    column :id
    column :state_id
    column :name
    column :latitude
    column :longitude
    actions
  end

  filter :state_id
  filter :name

  form do |f|
    f.inputs "New Township" do
      f.input :state_id, :as => :select, collection: StateDivision.all.map {|s| [s.name, s.id]}
      f.input :name
      f.input :latitude
      f.input :longitude
    end
    f.actions
  end

end
