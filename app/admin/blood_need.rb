ActiveAdmin.register BloodNeed do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
permit_params :location, :phone_no, :about, :blood_type_id, :blood_rh_type_id, :latitude, :longitude

  index do
    selectable_column
    id_column
    column :id
    column :location
    column :phone_no
    column :about
    column :blood_type_id
    column :blood_rh_type_id
    column :latitude
    column :longitude
    actions
  end

  filter :location
  filter :phone_no
  filter :about
  filter :blood_type_id
  filter :blood_rh_type_id

  form do |f|
    f.inputs "New Blood Need" do
      f.input :location
      f.input :phone_no
      f.input :about
      f.input :blood_type_id, :as => :select, collection: BloodType.all.map {|b| [b.name, b.id]}
      f.input :blood_rh_type_id, :as => :select, collection: BloodRhType.all.map {|b| [b.name, b.id]}
      f.input :latitude
      f.input :longitude
    end
    f.actions
  end

end
