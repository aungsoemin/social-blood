json.array! @requests do |request|

  json.request_sender do
    json.id request.request_sender.id
    json.name request.request_sender.name
    json.phone_no request.request_sender.phone_no
    json.latitude request.request_sender.latitude
    json.longitude request.request_sender.longitude
    json.profile_photo_url request.request_sender.profile_photo.present? ? request.request_sender.profile_photo.url : ''
    json.profile_photo_thumb_url request.request_sender.profile_photo.present? ? request.request_sender.profile_photo.thumb.url : ''
    json.blood_type_id request.request_sender.blood_type_id
    json.blood_rh_type_id request.request_sender.blood_rh_type_id
    json.address request.request_sender.address
  end
  json.accept_flg request.accept_flg

end
