json.id @user.id
json.name @user.name
json.email @user.email
json.phone_no @user.phone_no.present? ? @user.phone_no : ''
json.birth_date @user.birth_date.present? ? @user.birth_date : ''
json.gender @user.gender.present? ? @user.gender : ''
json.profile_photo_url @user.profile_photo.present? ? @user.profile_photo.url : ''
json.profile_photo_thumb_url @user.profile_photo.present? ? @user.profile_photo.thumb.url : ''
json.blood_type_id @user.blood_type_id.present? ? @user.blood_type_id : 0
json.blood_rh_type_id @user.blood_rh_type_id.present? ? @user.blood_rh_type_id : 0
json.fb_id @user.fb_id.present? ? @user.fb_id : ''
json.auth_token @user.auth_token
json.latitude @user.latitude.present? ? @user.latitude : 0
json.longitude @user.longitude.present? ? @user.longitude : 0
json.address @user.address.present? ? @user.address : ''
json.about @user.about.present? ? @user.about : ''
json.has_pet @user.has_pet.present? ? @user.has_pet : false
json.pet @user.pet.present? ? @user.pet : ''
