json.array! @blood_need do |b|
  json.distance b.distance.present? ? b.distance : ''
  json.city_name b.city_name.present? ? b.city_name : ''
  json.phone_no b.phone_no
  json.about b.about
  json.blood_type_id b.blood_type_id
  json.blood_rh_type_id b.blood_rh_type_id
  json.created_at b.created_at
end
