class Api::BloodRequestsController < Api::BaseController
  skip_before_filter :verify_authenticity_token
  before_filter :check_auth_token

  def create
    @request = BloodRequest.where("user_id = ? AND request_sender_id = ?",params[:user_id],params[:request_sender_id]).first
    return render json: { message: "You have already requested this user! " }, status: 400  if @request.present?

    @request = BloodRequest.new(
        user_id: params[:user_id],
        request_sender_id: params[:request_sender_id],
        accept_flg: false
      )

    if @request.save!
      return render json: { message: "Requested"} , status: 200
    else
      return render json: { message: "#{@request.errors.first.first} #{@request.errors.first.second}" }, status: 404
    end
  end

end
