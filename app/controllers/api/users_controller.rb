class Api::UsersController < Api::BaseController
  skip_before_filter :verify_authenticity_token
  before_filter :check_auth_token

  def show
    @user = User.where( id: params[:id]).first
    return render json: { message: 'Invalid User'}, status: 400 if @user.blank?
    respond_success
  end


  def update
    @user = User.where( id: params[:id]).first
    return render json: { message: 'Invalid User'}, status: 400 if @user.blank?

    # @user.validate_fields = true
    # implement to base64 code for profile picture
    profile = params[:profile_photo]
    if profile.present?

      data_url = profile
      data_type = data_url.split(',')

        # Check which decode method to use
      if ( data_type[0] == 'data:image/jpeg;base64' )
        png = Base64.decode64(data_url['data:image/jpeg;base64,'.length .. -1])
      elsif ( data_type[0] == 'data:image/png;base64' )
        png = Base64.decode64(data_url['data:image/png;base64,'.length .. -1])
      else
        return render json: { message: "image type not supported" }, status: 400
      end

      tmp_file_name = Time.now.to_s << 'report-image.png'

      File.open('public/' + tmp_file_name, 'wb') { |f| f.write(png) }

      image = MiniMagick::Image.open('public/'+ tmp_file_name)

      @path = "public/"

      @file_name = 'thumb'

      image.write(@path + "#{@file_name}.jpg")

      @data = File.open("#{@path}#{@file_name}.jpg", "rb")

      # create the ReportImage obj because '@reports.create' knew only the string
      @user.profile_photo = @data


      # delete the temp files
      if ( @data )
      File.delete(Rails.root + "#{@path}#{@file_name}.jpg")
      File.delete(Rails.root + "#{@path}#{tmp_file_name}") if tmp_file_name.present?
      end
    end
    if @user.update(permit_params)
      respond_to do |format|
        format.html
        format.json { render json: @user, status: 200}
      end
    else
      return render json: { message: @user.errors} ,status: 404
    end
  end

  def update_name
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?

    return render json: { message: "Required Name"},status: 400 if params[:name].blank?
    if @user.update(params.permit(:name))
      respond_success
    else
      respond_fail
    end
  end

  def update_phone_no
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?

    return render json: { message: "Required Phone Number"},status: 400 if params[:phone_no].blank?
    if @user.update(params.permit(:phone_no))
      respond_success
    else
      respond_fail
    end
  end

  def update_birth_date
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?
    if @user.update(params.permit(:birth_date))
      respond_success
    else
      respond_fail
    end
  end

  def update_gender
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?
    if @user.update(params.permit(:gender))
      respond_success
    else
      respond_fail
    end
  end

  def update_profile_photo
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?

    # @user.validate_fields = true
    # implement to base64 code for profile picture
    profile = params[:profile_photo]
    if profile.present?

      data_url = profile
      data_type = data_url.split(',')

        # Check which decode method to use
      if ( data_type[0] == 'data:image/jpeg;base64' )
        png = Base64.decode64(data_url['data:image/jpeg;base64,'.length .. -1])
      elsif ( data_type[0] == 'data:image/png;base64' )
        png = Base64.decode64(data_url['data:image/png;base64,'.length .. -1])
      else
        return render json: { message: "image type not supported" }, status: 400
      end

      tmp_file_name = Time.now.to_s << 'report-image.png'

      File.open('public/' + tmp_file_name, 'wb') { |f| f.write(png) }

      image = MiniMagick::Image.open('public/'+ tmp_file_name)

      @path = "public/"

      @file_name = 'thumb'

      image.write(@path + "#{@file_name}.jpg")

      @data = File.open("#{@path}#{@file_name}.jpg", "rb")

      # create the ReportImage obj because '@reports.create' knew only the string
      @user.profile_photo = @data


      # delete the temp files
      if ( @data )
      File.delete(Rails.root + "#{@path}#{@file_name}.jpg")
      File.delete(Rails.root + "#{@path}#{tmp_file_name}") if tmp_file_name.present?
      end
    end
    if @user.update(params.permit(:profile_photo))
      respond_success
    else
      return render json: { message: @user.errors} ,status: 404
    end
  end

  def update_blood_type
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?
    if @user.update(blood_type_params)
      respond_success
    else
      respond_fail
    end
  end

  def update_address
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?

    return render json: { message: "Require Address"}, status: 400 if params[:address].blank?
    if @user.update(params.permit(:address))
      respond_success
    else
      respond_fail
    end
  end

  def update_about
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?
    if @user.update(params.permit(:about))
      respond_success
    else
      respond_fail
    end
  end

  def update_pet
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?
    if @user.update(pet_params)
      respond_success
    else
      respond_fail
    end
  end

  def destroy_profile_photo
    @user = User.where(id: params[:id]).first
    return render json: {message: "Invalid user"}, status: 400 if @user.blank?

    @user.remove_profile_photo!
    if @user.save
      respond_success
    else
      return render json: @user.errors, status: 404
    end
  end

  def update_location
    @user = User.where( id: params[:id]).first
    return render json: { message: "Invalid user"}, status: 400 if @user.blank?
    params[:latitude] = 0.0 if params[:latitude].blank?
    params[:longitude] = 0.0 if params[:longitude].blank?
    if @user.update(location_params)
      respond_success
    else
      respond_fail
    end
  end

  def respond_success
    respond_to do |format|
        format.html
        format.json{ render json: @user, status: 200}
    end
  end

  def respond_fail
    return render json: { message: @user.errors}, status: 404
  end

  # show the reveived requests
  def blood_requests
    @user = User.where( id: params[:id]).first
    return render json: { message: "Invalid User" }, status: 400 if @user.blank?
    @requests = BloodRequest.where(user_id: params[:id]).order(:accept_flg,:created_at)
  end

  #user accept request
  def request_accept
    @request = BloodRequest.where("user_id = ? AND request_sender_id = ?",params[:user_id],params[:request_sender_id]).first
    return render json: { message: "Invalid request! "}, status: 400 if @request.blank?

    unless @request.accept_flg
      @request.accept_flg = true
      if @request.save!
        return render json: { message: "Request Accepted! "}, status: 200
      else
        return render json: { message: @request.errors }, status: 404
      end
    else
      return render json: { message: "The request has already accepted! "}, status: 400
    end
  end

  #user deny request
  def request_deny
    @request = BloodRequest.where("user_id = ? AND request_sender_id = ?",params[:user_id],params[:request_sender_id]).not_accepted.first
    return render json: { message: "Invalid request! "}, status: 400 if @request.blank?

    if @request.destroy!
      return render json: { message: "Request Denied! "}, status: 200
    else
      return render json: { message: @request.errors}, status: 404
    end
  end


  private
    def permit_params
      params.permit(:name, :phone_no, :birth_date, :gender, :profile_photo, :blood_type_id, :blood_rh_type_id, :about, :has_pet, :pet)
    end

    def blood_type_params
      params.permit( :blood_type_id, :blood_rh_type_id)
    end

    def location_params
      params.permit( :latitude, :longitude)
    end

    def pet_params
      params.permit( :has_pet, :pet )
    end
end
