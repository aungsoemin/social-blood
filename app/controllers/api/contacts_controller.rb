class Api::ContactsController < Api::BaseController
  skip_before_filter :verify_authenticity_token
  before_filter :check_auth_token
  # API to lookup blood type based on array of FB IDs
  def fb_lookup
    # split incoming comma separated values into an array
    if params[:fb_ids].present?
      fb_ids = params[:fb_ids].split ","

      @users = User.where(:fb_id => fb_ids)

      if @users.count == 0
        return render nothing: true, status: 204
      else
        if current_user.latitude.blank? || current_user.latitude.nil? || current_user.longitude.blank? || current_user.longitude.nil?
          return render json: {message: "You need to update your location!"}, status: 400
        else
          @users.each do |user|
            if user.latitude.blank?
              user.latitude = 0.0
            end
            if user.longitude.blank?
                user.longitude =0.0
            end
            user.distance = Geocoder::Calculations.distance_between([current_user.latitude, current_user.longitude],[user.latitude, user.longitude])
            query = "#{user.latitude},#{user.longitude}"
            first_result = Geocoder.search(query).first
            if first_result.present?
              user.city_name = first_result.city
            end
          end
          @users = @users.sort_by{|data| data.distance}
          respond_to do |format|
            format.html
            format.json { render json: @users, status: 200}
          end
        end
      end
    else
      return render nothing: true, status: 204
    end

  end

  # API to lookup blood type based on array of contact IDs
  def contacts_lookup
    # split incoming comma separated values into an array
    if params[:contacts].present?
      @users = User.matched_contacts(params[:contacts])
      if @users.count == 0
        return render nothing: true, status: 204
      else
        if current_user.latitude.blank? || current_user.latitude.nil? || current_user.longitude.blank? || current_user.longitude.nil?
          return render json: {message: "You need to update your location!"}, status: 400
        else
          @users.each do |user|
            if user.latitude.blank?
              user.latitude = 0.0
            end
            if user.longitude.blank?
              user.longitude =0.0
            end
            user.distance = Geocoder::Calculations.distance_between([current_user.latitude, current_user.longitude],[user.latitude, user.longitude])
            query = "#{user.latitude},#{user.longitude}"
            first_result = Geocoder.search(query).first
            if first_result.present?
              user.city_name = first_result.city
            end
          end
        end
        @users = @users.sort_by{|data| data.distance}
        respond_to do |format|
          format.html
          format.json { render json: @users, status: 200}
        end
      end
    end
  end
end
