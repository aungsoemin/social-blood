class Api::BaseController < ActionController::Base
  skip_before_filter :verify_authenticity_token

  respond_to :json

  def check_auth_token
    if params[:auth_token].present?
      user = User.where(auth_token: params[:auth_token]).first

      if user.present?
        sign_in(user, store: false)
      else
        return render json: { message: "Invalid Auth Token!" }, status: 401
      end
    else
      return render json: { message: "Require Auth Token!" }, status: 422
    end
  end
end
