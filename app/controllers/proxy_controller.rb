class ProxyController < ActionController::Base
  def call
    redirect_to "tel://#{params[:phone]}"
  end

  def sms
    redirect_to "sms://#{params[:phone]}"
  end
end
