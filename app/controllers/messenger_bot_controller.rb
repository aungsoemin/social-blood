class MessengerBotController < ActionController::Base
  Messenger::Bot.on("message") do |event, sender|
    puts "Received - #{event}"
    FacebookBot.handle_incoming_message(event, sender)
  end
  Messenger::Bot.on("delivery") do |event, sender|
    puts "Delivered - #{event}"
  end
  Messenger::Bot.on("postback") do |event, sender|
    puts "Posted Back - #{event}"
    FacebookBot.handle_payload(event, sender)
  end
end
