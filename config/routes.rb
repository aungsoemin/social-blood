Rails.application.routes.draw do

  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  mount Messenger::Bot::Space => "/webhook"

  get 'call/:phone', to: 'proxy#call'
  get 'sms/:phone', to: 'proxy#sms'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  namespace :api do
      # devise_for :users
      devise_scope :user do
        post 'users/sign_up' , to: 'registrations#sign_up'
        post 'users/login' , to:  'sessions#create'
        post 'users/fb_login', to: 'registrations#fb_login'
        post 'users/fb_update', to: 'registrations#fb_update'
        post 'users/ak_login', to: 'registrations#ak_login'
        post'users/ak_signup', to: 'registrations#ak_signup'
        delete 'users/logout' , to:  'sessions#destroy'
        post 'users/forgot_password' , to: 'sessions#forgot_password'

      end

      resources :users, :only => [:show, :update] do
        collection do
            put ':id/update_name', to: 'users#update_name'
            put ':id/update_phone_no', to: 'users#update_phone_no'
            put ':id/update_birth_date', to: 'users#update_birth_date'
            put ':id/update_gender', to: 'users#update_gender'
            put ':id/update_profile_photo', to: 'users#update_profile_photo'
            put ':id/update_blood_type', to: 'users#update_blood_type'
            put ':id/update_address', to: 'users#update_address'
            put ':id/update_about', to: 'users#update_about'
            put ':id/update_pet', to: 'users#update_pet'
            put ':id/update_location', to: 'users#update_location'
            get ':id/blood_requests', to: 'users#blood_requests'
            put ':id/request_accept', to: 'users#request_accept'
            put ':id/request_deny', to: 'users#request_deny'
            delete ':id/destroy_profile_photo', to: 'users#destroy_profile_photo'
        end
      end

      resources :blood_needs, :only => [:index, :show, :create]
      get 'search', to: 'search#index'
      post 'fb/lookup', to: 'contacts#fb_lookup'
      post 'contacts/lookup', to: 'contacts#contacts_lookup'
      resources :blood_banks, :only => [:index]
      resources :blood_requests, :only => [:create]

  end
end
