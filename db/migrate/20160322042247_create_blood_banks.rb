class CreateBloodBanks < ActiveRecord::Migration
  def change
    create_table :blood_banks do |t|
      t.string :name
      t.string :phone_no
      t.string :address
      t.float :latitude
      t.float :longitude

      t.timestamps null: false
    end
  end
end
