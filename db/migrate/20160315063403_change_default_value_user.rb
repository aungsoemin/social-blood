class ChangeDefaultValueUser < ActiveRecord::Migration
  def change
    change_column :users, :name, :string, :default => ""
    change_column :users, :phone_no, :string, :default => ""
    change_column :users, :blood_type_id, :integer, :default => 0
    change_column :users, :blood_rh_type_id, :integer, :default => 0
    change_column :users, :latitude, :float, :default => 0
    change_column :users, :longitude, :float, :default => 0
    change_column :users, :address, :string, :default => ""
  end
end
