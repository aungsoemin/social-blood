class AddAddressRemoveTownshipUser < ActiveRecord::Migration
  def change
    add_column :users, :address , :string
    remove_column :users, :state_id, :integer
    remove_column :users, :township_id, :integer
  end
end
